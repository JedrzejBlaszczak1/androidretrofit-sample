package com.jblaszczak.retrofitsample.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.jblaszczak.retrofitsample.database.OrdersRoomDatabase
import com.jblaszczak.retrofitsample.domain.OrderItem
import com.jblaszczak.retrofitsample.network.OrdersApi
import com.jblaszczak.retrofitsample.utils.asDomainOrderModel
import com.jblaszczak.retrofitsample.utils.convertToDbData
import com.jblaszczak.retrofitsample.utils.convertToDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.net.SocketTimeoutException
import javax.inject.Inject

class OrdersRepository @Inject constructor(private var database: OrdersRoomDatabase) {

    suspend fun refreshOrdersData() {
        withContext(Dispatchers.IO) {
            try {
                Timber.d("refreshOrdersData()")
                val response = OrdersApi.ordersApiService.getOrders()
                database.orderDao.insertAll(convertToDbData(response))
            }
            catch(exception: SocketTimeoutException){
                Timber.e("SocketTimeout")
            }
        }
    }

    fun getOrder(id: Int): LiveData<OrderItem> {
        return Transformations.map(database.orderDao.getOrder(id)) {
            convertToDomainModel(it)
        }
    }

    val ordersList: LiveData<List<OrderItem>> =
        Transformations.map(database.orderDao.getAllOrders()) {
            it?.asDomainOrderModel()
        }
}