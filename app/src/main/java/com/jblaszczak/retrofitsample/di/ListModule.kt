package com.jblaszczak.retrofitsample.di

import com.jblaszczak.retrofitsample.repository.OrdersRepository
import com.jblaszczak.retrofitsample.ui.list.ListViewModel
import com.jblaszczak.retrofitsample.ui.list.OrderAdapter
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object ListModule {
    @Provides
    fun provideViewModel(repository: OrdersRepository): ListViewModel {
        return ListViewModel(repository)
    }

    @Provides
    fun provideOrderAdapter(): OrderAdapter {
        return OrderAdapter()
    }
}