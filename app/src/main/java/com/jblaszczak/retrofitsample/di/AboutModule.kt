package com.jblaszczak.retrofitsample.di

import com.jblaszczak.retrofitsample.ui.about.AboutViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
object AboutModule {
    @Provides
    fun provideViewModel(): AboutViewModel {
        return AboutViewModel()
    }
}