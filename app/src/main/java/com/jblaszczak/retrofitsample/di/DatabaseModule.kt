package com.jblaszczak.retrofitsample.di

import android.content.Context
import com.jblaszczak.retrofitsample.database.OrderDao
import com.jblaszczak.retrofitsample.database.OrdersRoomDatabase
import com.jblaszczak.retrofitsample.repository.OrdersRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideRepository(database: OrdersRoomDatabase): OrdersRepository {
        return OrdersRepository(database)
    }

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext application: Context): OrdersRoomDatabase {
        return OrdersRoomDatabase.getDatabase(application)
    }

    @Provides
    fun provideOrderDao(database: OrdersRoomDatabase): OrderDao {
        return database.orderDao
    }
}