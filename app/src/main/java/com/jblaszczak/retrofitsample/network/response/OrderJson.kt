package com.jblaszczak.retrofitsample.network.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderJson(
    @Json(name = "description")
    val description: String,
    @Json(name = "image_url")
    val imageUrl: String,
    @Json(name = "modificationDate")
    val modificationDate: String,
    @Json(name = "orderId")
    val orderId: Int,
    @Json(name = "title")
    val title: String
)