package com.jblaszczak.retrofitsample.network

import com.jblaszczak.retrofitsample.network.response.OrderJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

const val BASE_URL = "https://recruitment-task.futuremind.dev/"

interface OrdersApiService {

    @GET("recruitment-task")
    suspend fun getOrders(): List<OrderJson>
}

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

object OrdersApi {
    val ordersApiService: OrdersApiService by lazy {
        retrofit.create(OrdersApiService::class.java)
    }
}

