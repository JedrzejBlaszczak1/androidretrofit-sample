package com.jblaszczak.retrofitsample.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "orders")
data class Order(
        @PrimaryKey
        val id: Int,
        val title: String,
        val modificationDate: String,
        val imageUrl: String,
        val description: String
)