package com.jblaszczak.retrofitsample.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface OrderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(ordersList: List<Order>)

    @Query("select * from orders where id = :id")
    fun getOrder(id: Int): LiveData<Order>

    @Query("select * from orders")
    fun getAllOrders(): LiveData<List<Order>>

    @Query("delete from orders")
    fun deleteAll()
}

@Database(entities = [Order::class], version = 1)
abstract class OrdersRoomDatabase : RoomDatabase() {

    abstract val orderDao: OrderDao

    companion object {
        @Volatile
        private var INSTANCE: OrdersRoomDatabase? = null

        fun getDatabase(context: Context): OrdersRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        OrdersRoomDatabase::class.java,
                        "orders_database"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}