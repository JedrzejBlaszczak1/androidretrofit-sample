package com.jblaszczak.retrofitsample

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RetrofitsampleApplication : Application() {}