package com.jblaszczak.retrofitsample.utils

import com.jblaszczak.retrofitsample.database.Order
import com.jblaszczak.retrofitsample.domain.OrderItem
import com.jblaszczak.retrofitsample.network.response.OrderJson

fun convertToDbData(response: List<OrderJson>): List<Order> {
    val tempList: MutableList<Order> = mutableListOf()

    for (item in response) {
        tempList.add(
            Order(
                item.orderId,
                item.title,
                item.modificationDate,
                item.imageUrl,
                item.description
            )
        )
    }

    tempList.sortBy { it.id }

    return tempList
}

fun convertToDomainModel(order: Order): OrderItem {
    return OrderItem(
        id = order.id,
        title = order.title,
        modificationDate = order.modificationDate,
        imageUrl = order.imageUrl,
        description = order.description
    )
}

fun List<Order>.asDomainOrderModel(): List<OrderItem> {
    return map {
        convertToDomainModel(it)
    }
}