package com.jblaszczak.retrofitsample.ui.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jblaszczak.retrofitsample.repository.OrdersRepository
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ListViewModel @Inject constructor(private var repository: OrdersRepository) : ViewModel() {

    val ordersList = repository.ordersList

    init {
        Timber.d("init")
        if(ordersList.value.isNullOrEmpty())
            refreshOrdersList()
    }

    fun refreshOrdersList() {
        viewModelScope.launch {
            repository.refreshOrdersData()
        }
    }

    fun getOrderUrl(position: Int): String {
        val description = ordersList.value!![position].description
        val pattern =
            """(?i)<a\s+[^>]*>[^<]*</a>|(https?|ftp)://(?:-\.)?([^\s/?.#-]+\.?)+(/\S*)?""".toRegex()

        return pattern.find(description)?.value ?: ""
    }
}