package com.jblaszczak.retrofitsample.ui.list.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.jblaszczak.retrofitsample.R
import com.jblaszczak.retrofitsample.databinding.FragmentOrderDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class OrderDetailsFragment @Inject constructor() : Fragment() {

    private var orderUrl: String? = null
    private lateinit var binding: FragmentOrderDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Timber.d("OnCreateView()")
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false)

        orderUrl = arguments?.getString(ARG_ORDER_URL)

        class CustomWebViewClient : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                binding.progressBar.visibility = View.GONE
            }
        }

        val webView = binding.detailsWebView
        webView.loadUrl(orderUrl ?: "")
        webView.webViewClient = CustomWebViewClient()
        webView.webChromeClient = WebChromeClient()

        return binding.root
    }

    companion object {
        const val ARG_ORDER_URL = "ORDER_URL"
    }
}