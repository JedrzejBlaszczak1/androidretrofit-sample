package com.jblaszczak.retrofitsample.ui.list.details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.jblaszczak.retrofitsample.R
import com.jblaszczak.retrofitsample.databinding.ActivityOrderDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OrderDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOrderDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_details)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            val fragment = OrderDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(OrderDetailsFragment.ARG_ORDER_URL,
                        intent.getStringExtra(OrderDetailsFragment.ARG_ORDER_URL))
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.order_detail_container, fragment)
                .commit()
        }
    }
}