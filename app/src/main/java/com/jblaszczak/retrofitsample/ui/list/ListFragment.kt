package com.jblaszczak.retrofitsample.ui.list

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jblaszczak.retrofitsample.R
import com.jblaszczak.retrofitsample.databinding.FragmentListBinding
import com.jblaszczak.retrofitsample.ui.list.details.OrderDetailsActivity
import com.jblaszczak.retrofitsample.ui.list.details.OrderDetailsFragment
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class ListFragment @Inject constructor() : Fragment(), OrderAdapter.OnOrderListener {
    private lateinit var binding: FragmentListBinding

    @Inject
    lateinit var viewModel: ListViewModel

    @Inject
    lateinit var adapter: OrderAdapter

    private var twoPane: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Timber.d("onCreateView() called")

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false)

        if (binding.includeRecyclerList.itemDetailContainer != null) {
            twoPane = true
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated() called")

        binding.includeRecyclerList.recyclerList.layoutManager = LinearLayoutManager(context)
        binding.includeRecyclerList.recyclerList.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        adapter.setOnOrderListener(this)
        binding.includeRecyclerList.recyclerList.adapter = adapter

        viewModel.ordersList.observe(viewLifecycleOwner) { orders ->
            orders.let {
                Timber.d("viewModel.ordersList.observe()")
                adapter.updateItems(orders)
            }
        }

        binding.includeRecyclerList.swipeContainer.setOnRefreshListener {
            viewModel.refreshOrdersList()
            binding.includeRecyclerList.swipeContainer.isRefreshing = false
        }
    }

    override fun onOrderClick(position: Int) {
        val orderUrl = viewModel.getOrderUrl(position)
        if (twoPane) {
            val fragment = OrderDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(OrderDetailsFragment.ARG_ORDER_URL, orderUrl)
                }
            }

            requireActivity().supportFragmentManager
                .beginTransaction()
                .replace(R.id.item_detail_container, fragment)
                .commit()
        } else {
            val intent = Intent(context, OrderDetailsActivity::class.java)
            intent.putExtra(OrderDetailsFragment.ARG_ORDER_URL, orderUrl)
            startActivity(intent)
        }
    }
}