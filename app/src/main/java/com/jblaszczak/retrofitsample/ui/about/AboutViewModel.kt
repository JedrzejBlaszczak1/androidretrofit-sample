package com.jblaszczak.retrofitsample.ui.about

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class AboutViewModel @Inject constructor() : ViewModel()