package com.jblaszczak.retrofitsample.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jblaszczak.retrofitsample.databinding.OrderListItemBinding
import com.jblaszczak.retrofitsample.domain.OrderItem
import timber.log.Timber

class OrderAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val items = ArrayList<OrderItem>()
    private lateinit var onOrderListener: OnOrderListener

    fun setOnOrderListener(listener: OnOrderListener) {
        this.onOrderListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return ViewHolderOrder(OrderListItemBinding.inflate(inflater), onOrderListener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderOrder).bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun updateItems(newItems: List<OrderItem>) {
        Timber.d("updateItems() : Items count to be added = %s", newItems.size)
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    inner class ViewHolderOrder(
        val binding: OrderListItemBinding,
        val newOnOrderListener: OnOrderListener
    ) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        lateinit var onOrderListener: OnOrderListener

        fun bind(order: OrderItem?) {
            binding.order = order

            onOrderListener = newOnOrderListener
            binding.layoutItem.setOnClickListener(this)

            binding.executePendingBindings()
        }

        override fun onClick(p0: View?) {
            onOrderListener.onOrderClick(adapterPosition)
        }
    }

    interface OnOrderListener {
        fun onOrderClick(position: Int)
    }
}