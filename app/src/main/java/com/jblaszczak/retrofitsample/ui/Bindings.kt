package com.jblaszczak.retrofitsample.ui

import android.graphics.BitmapFactory
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.net.URL
import java.time.LocalDate

class Bindings {
    companion object {
        @BindingAdapter("orderTitleValue")
        @JvmStatic
        fun setOrderTitle(textView: TextView, title: String?) {
            if (title != null) {
                textView.text = title
            }
        }

        @BindingAdapter("orderDateValue")
        @JvmStatic
        fun setOrderDateValue(textView: TextView, date: String?) {
            if (date != null) {
                val localDate = LocalDate.parse(date)
                """${localDate.dayOfMonth} ${localDate.month} ${localDate.year}""".also { textView.text = it }
            }
        }

        @BindingAdapter("orderDescriptionValue")
        @JvmStatic
        fun setOrderDescriptionValue(textView: TextView, description: String?) {
            if (description != null) {
                val pattern =
                    """(?i)<a\s+[^>]*>[^<]*</a>|(https?|ftp)://(?:-\.)?([^\s/?.#-]+\.?)+(/\S*)?""".toRegex()
                textView.text = description.replace(pattern) {
                    if (it.groupValues[1].isEmpty()) it.value else ""
                }
            }
        }

        @BindingAdapter("orderImageResource")
        @JvmStatic
        fun setOrderImageResource(imageView: ImageView, url: String?) {
            GlobalScope.launch(Dispatchers.IO) {
                Timber.d(url)
                //URL provided with the image_url does not work, as the provided hosting is most likely very slow
                //Random image used for now
                val imageUrl =
                    URL("https://cdn.pixabay.com/photo/2021/01/08/17/56/river-5900547_960_720.jpg")
                val bmp = BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream())

                launch(Dispatchers.Main) {
                    imageView.setImageBitmap(bmp)
                }
            }

        }
    }
}