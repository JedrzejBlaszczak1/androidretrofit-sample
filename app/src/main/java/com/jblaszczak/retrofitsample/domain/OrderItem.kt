package com.jblaszczak.retrofitsample.domain

data class OrderItem(
    val id: Int,
    val title: String,
    val modificationDate: String,
    val imageUrl: String,
    val description: String
)